package com.stock.mvc.dao.impl;

import java.io.InputStream;

import javax.swing.JOptionPane;


import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuth1Token;
import com.stock.mvc.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao{

	private Flickr flickr;

	private UploadMetaData uploadMetaData = new UploadMetaData();

	private String apiKey = "0da853c2f4775f7e441e0a07590997e8";

	private String sharedSecret = "aa82fc8e45f574cb";

	private void connect(){
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157713925527787-94e00c28fa5488d4");
		auth.setTokenSecret("d20daf08e9cefefb");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}

	@Override
	public String savePhoto(InputStream photo, String title) throws Exception{
		connect();
		uploadMetaData.setTitle(title);
		String photoId = flickr.getUploader().upload(photo, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}

	 public void auth(){
	        flickr = new Flickr(apiKey, sharedSecret, new REST());

	        AuthInterface authInterface = flickr.getAuthInterface();

	        OAuth1RequestToken token = authInterface.getRequestToken();
	        System.out.println("token: " + token);

	        String url = authInterface.getAuthorizationUrl((OAuth1RequestToken) token, Permission.DELETE);
	        System.out.println("Follow this URL to authorise yourself on Flickr");
	        System.out.println(url);
	        System.out.println("Paste in the token it gives you:");
	        System.out.print(">>");

	        String tokenKey = JOptionPane.showInputDialog(null);

	        OAuth1Token requestToken = authInterface.getAccessToken(token, new String(tokenKey));
	        System.out.println("Authentication success");

	        Auth auth = null;
	        try {
	            auth = authInterface.checkToken(requestToken);
	        } catch (FlickrException e) {
	            e.printStackTrace();
	        }

	        // This token can be used until the user revokes it.
	        System.out.println("Token: " + requestToken.getToken());
	        System.out.println("Secret: " + requestToken.getTokenSecret());
	        System.out.println("nsid: " + auth.getUser().getId());
	        System.out.println("Realname: " + auth.getUser().getRealName());
	        System.out.println("Username: " + auth.getUser().getUsername());
	        System.out.println("Permission: " + auth.getPermission().getType());
	 }


}
